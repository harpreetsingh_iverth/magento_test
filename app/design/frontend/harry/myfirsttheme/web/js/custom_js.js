
  $('.slider4').bxSlider({
    slideWidth: 300,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10
  });

  (function(){
 
  jQuery('#carouselABC').carousel({ interval: 3600 });
}());

(function(){
  jQuery('.carousel-showmanymoveone .item').each(function(){
    var itemToClone = $(this);

    for (var i=1;i<4;i++) {
      itemToClone = itemToClone.next();

      // wrap around if at end of item collection
      if (!itemToClone.length) {
        itemToClone = jQuery(this).siblings(':first');
      }

      // grab item, clone, add marker class, add to collection
      itemToClone.children(':first-child').clone()
        .addClass("cloneditem-"+(i))
        .appendTo(jQuery(this));
    }
  });
}());

      $(document).ready(function() {
        $(".navbar-toggle").click(function(){
          $(".mbl-menu-final").animate({right: '-30%'},"fast");
        });
        $(".close-mbl").click(function(){  
          HideSearch();
        });
    });
      function HideSearch()
      {
        $(".mbl-menu-final").animate({right: '-109%'},"fast");
      }
